<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Users extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username');
            $table->string('password');
            $table->enum('user_type',array('Admin','Student','Faculty'))->default('Student');
            $table->timestamps();
        });

        DB::table('users')->insert([
            'username' => 'admin',
            'password' => '$2y$10$KqOpY.qyukjy83NI1QU7EOmWBY7CmMAWMwG9nkHBqA9i43DfdQW.C',
            'user_type' => 'Admin'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
