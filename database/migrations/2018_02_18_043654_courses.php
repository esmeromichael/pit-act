<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Courses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('short');
            $table->string('name');
            $table->timestamps();
        });

        DB::table('courses')->insert([
            'short' => 'BSHTE',
            'name' => 'Bachelor of Science in Home Technology Education'
        ]);

        DB::table('courses')->insert([
            'short' => 'BEED',
            'name' => 'Bachelor of Elementary Education'
        ]);

        DB::table('courses')->insert([
            'short' => 'BSED',
            'name' => 'Bachelor of Secondary Education'
        ]);

        DB::table('courses')->insert([
            'short' => 'BSIT',
            'name' => 'Bachelor of Science in Industrial Technology'
        ]);

        DB::table('courses')->insert([
            'short' => 'BS Info Tech',
            'name' => 'Bachelor of Science in Information Technology'
        ]);

        DB::table('courses')->insert([
            'short' => 'BSHRM',
            'name' => 'Bachelor of Science in Hotel and Restaurant Management'
        ]);

        DB::table('courses')->insert([
            'short' => 'BSFi',
            'name' => 'Bachelor of Science in Fisheries'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('courses');
    }
}
