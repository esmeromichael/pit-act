<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EnrollDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enroll_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('header_id');
            $table->integer('course_id');
            $table->string('course_no');
            $table->string('title');
            $table->decimal('units',24,2)->default('0.00');
            $table->string('schedule');
            $table->string('room_no');
            $table->string('instructor');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('enroll_details');
    }
}
