<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EnrollHeaders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enroll_headers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('student_id');
            $table->enum('semester',array('First','Second'));
            $table->integer('sy1');
            $table->integer('sy2');
            $table->date('birth_date');
            $table->string('birth_place');
            $table->string('summer');
            $table->enum('civil_status',array('Single','Married','Separated','Widow'));
            $table->integer('course_id');
            $table->string('major');
            $table->enum('year',array('First','Second','Third','Fourth'));
            $table->string('contact_number');
            $table->string('credential');
            $table->string('school_attended');
            $table->string('attended_school_year');
            $table->string('guardian');
            $table->string('home_address');
            $table->string('contact_no');
            $table->string('tabango_address');
            $table->string('notify');
            $table->date('reg_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('enroll_headers');
    }
}
