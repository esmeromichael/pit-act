<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AcctHeader extends Model
{
    public function student(){
    	return $this->belongsTo('App\Student');
    }
    static function details($id){
    	return AcctDetail::leftJoin('misc_fees','acct_details.misc_id','=','misc_fees.id')
    			->select('acct_details.*','misc_fees.name as misc_name')
    			->where('acct_details.header_id',$id)->get();
    }
}
