<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EnrollDetail extends Model
{
    public function subject(){
    	return $this->belongsTo('App\Subject','course_id');
    }
}
