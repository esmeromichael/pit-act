<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

use App\Http\Controllers\Controller;
use Illuminate\Pagination\BootstrapThreePresenter;

use App\User;
use Response;
use App\PatientInfo;
use App\Course;
use App\Year;
use App\StatusType;
use Request;
use Input;
use Redirect;
use Session;
use App\ExamType;
use App\Consultation;
use App\ConsultType;
use App\InfoHeader;
use App\InfoDetail;
use DB;


class Controller extends BaseController
{
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;

    public function editconsult($id){
        if(Session::get('user_id')){
            $exam_type = Consultation::where('status','Active')->get();
	        $type = ExamType::where('status','Active')->get();
	        $consult = ConsultType::where('status','Active')->get();
	        $tab = ['tab' => 'Consultation', 'subtab' => ''];
	        return view('/webpage/editconsult',compact('exam_type','type','consult','tab'));
        }
        else{
            Session::flush();
            return redirect()->action('WelcomeController@checklogin');
        }
    }
}
