<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        if(Session::get('user_id')){
            $tab = ['tab' => 'Home', 'subtab' => ''];
            return view('home',compact('tab'));
        }
        else{
            Session::flush();
            return redirect()->action('WelcomeController@checklogin');
        }
    }
}
