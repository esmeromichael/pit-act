<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Input;
use Session;
use App\User;
use Hash;

class WelcomeController extends Controller
{
    public function viewloginpage(){
    	return view('auth/logins');
    }
    public function login(){
		
		if(Input::get('logins')){
			$username = Input::get('username');
			$password = Input::get('password');
			$user = User::where('username',$username)->first();
			if(!$user){
				return redirect()->action('WelcomeController@checklogin')->with('error','Invalid Account');
			}
			else{
				if(Hash::check($password, $user->password)){
					Session::put('user_id', $user->id);
					Session::save();
					if($user){
						return redirect('/home');
					}
					else{
						Session::flush();
						return redirect()->action('WelcomeController@checklogin');
					}
				}
				else{
					return redirect()->action('WelcomeController@checklogin')->with('error','Invalid Password');
				}
			}
			return Session::get('user_id');
		}
	}

	public function checklogin(){
		$user_id = Session::get('user_id');
		if($user_id){ 
			$user = User::where('id',$user_id)->first();
			if($user->id){
				return redirect('/home');
			} 
			else{
				Session::flush();
				return redirect()->action('WelcomeController@checklogin');
			}
		} 
		else{
			return view('auth/logins');
		} 
	}

	public function logout(){
		Session::flush();
		return redirect()->action('WelcomeController@checklogin');
	}
}
