<?php

namespace App\Http\Controllers\Info;


// use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

use App\Http\Controllers\Controller;
use Illuminate\Pagination\BootstrapThreePresenter;

//use Illuminate\Http\Request; 


use App\User;
use Response;
use App\Course;
use Request;
use Input;
use Redirect;
use Session;
use DB;
use PDF;
use App\Student;
use App\MiscFee;
use App\AcctHeader;
use App\AcctDetail;
use App\Receipt;
use App\Subject;
use App\Grade;
use App\EnrollHeader;
use App\EnrollDetail;

class InfoController extends Controller
{
    public function getIndex(Request $request) {
        $request = app(\Illuminate\Http\Request::class);
        $students = Student::orderBy('lname')->paginate(10);
        $courses = Course::all();
        // $student = PatientInfo::where('status','Student')->orderBy('lname')->paginate(10);  
        $tab = ['tab' => 'Student', 'subtab' => 'index'];
        if(Session::get('user_id')){
            if ($request->ajax()) {
                return view('webpage/indexpage',compact('students','courses','tab'));
            }
            return view('webpage/index', compact('students','courses','tab'));
        }
        else{
            Session::flush();
            return redirect()->action('WelcomeController@checklogin');
        }
	}

	public function postIndex(){
        if(Input::get('addstudent')){
                $patient = new Student;
                $patient->student_id = Input::get('student_id');
                $patient->fname = Input::get('fname');
                $patient->mname = Input::get('mname');
                $patient->lname = Input::get('lname');
                $patient->name = Input::get('fname').' '.Input::get('mname').' '.Input::get('lname');
                $patient->course_id = Input::get('course');
                $patient->address = Input::get('address');
                $patient->year_level = Input::get('year');

                $patient->save();

                $fi = substr($patient->fname,0,2);
                $se = substr($patient->lname,0,2);
                $co = substr($patient->address,0,3);
                $uppa = Student::where('id',$patient->id)->first();
                $uppa->code = strtoupper($fi).strtoupper($se).strtoupper($co).$patient->id;
                $uppa->save();

                Session::flash('alert-success', 'Student successfully added.');
                return redirect()->action('Info\InfoController@getIndex', $patient->id);

        }
        else if(Input::get('edit_student')){
                $patient = Student::where('id',Input::get('edit_dtl_id'))->first();
                $patient->student_id = Input::get('edit_student_id');
                $patient->fname = Input::get('edit_fname');
                $patient->mname = Input::get('edit_mname');
                $patient->lname = Input::get('edit_lname');
                $patient->course_id = Input::get('edit_course');
                $patient->address = Input::get('edit_address');
                $patient->year_level = Input::get('edit_year');
                $patient->save();
            
                Session::flash('alert-success', 'Student successfully updated.');
                return redirect()->action('Info\InfoController@getIndex', $patient->id);
        }
	}

    public function getCourse(){
        $course = Course::all();
        return Response::json($course, 200, array(), JSON_PRETTY_PRINT);
    }

    public function accounting(){
        $tab = ['tab' => 'Accounting', 'subtab' => 'index'];
        $courses = Course::all();   
        $grades = Grade::leftJoin('students','grades.student_id','=','students.id')
                        ->leftJoin('courses','grades.course_id','=','courses.id')
                        ->select('grades.student_id as student_id','students.fname as fname','students.lname as lname',
                        'students.mname as mname','courses.short as coursename','grades.student_id as student_id','grades.year as year')
                        ->orderBy('students.lname','asc')
                        ->groupBy('grades.student_id')->paginate(10);
        if(Session::get('user_id')){
            return view('webpage/accounting', compact('tab','courses','grades'));
        }
        else{
            Session::flush();
            return redirect()->action('WelcomeController@checklogin');
        }
    }

    public function saveaccounting(){
        if(Input::get('addmisc')){
            $rows = count(Input::get('sub_id'));
            if($rows > 0){
                for($i=0;$i<$rows;$i++){
                    $new = new Grade;
                    $new->student_id = Input::get('studentid');
                    $new->course_id = Input::get('course');
                    $new->year = Input::get('year');
                    $new->subject_id = Input::get('sub_id')[$i];
                    $new->grade = Input::get('grade')[$i];
                    $new->save();
                }
            }
           
            Session::flash('alert-success', 'Grades successfully saved.');
            return redirect()->action('Info\InfoController@accounting');
        }
    }

    public function receiptprofile($id){
        $header = Receipt::where('id',$id)->first();
        $tab = ['tab' => 'Receipt', 'subtab' => 'index'];
        if(Session::get('user_id')){
            return view('webpage/receiptprofile', compact('tab','header'));
        }
        else{
            Session::flush();
            return redirect()->action('WelcomeController@checklogin');
        }
    }

    public function updateprofile($id){
        if(Input::get('update')){
            $header = Receipt::where('id',$id)->first();
            $header->bill_date = date('Y-m-d', strtotime(Input::get('rdate')));
            $header->receipt_no = Input::get('receipt_no');
            $header->amount_paid = Input::get('amount_paid');
            $header->save();

            $bill = AcctHeader::where('id',$header->bill_id)->first();
            $balance = $bill->balance + Input::get('original_amount');
            $credit = $bill->credit - Input::get('original_amount');
            $bill->credit = $credit +  Input::get('amount_paid');
            $bill->balance = $balance - Input::get('amount_paid');
            $bill->save();

            Session::flash('alert-success', 'Receipt successfully updated.');
            return redirect()->action('Info\InfoController@receiptprofile', $header->id);
        }
        else if(Input::get('print')){
            PDF::SetTitle('Receipt');
            PDF::SetFont('dejavusans', '', 10);
            PDF::SetMargins(15, 15, 0, true);
            //PDF::AddPage('L');
            //TCPDF::setEqualColumns(3,0,'');
            PDF::AddPage();

            $header = Receipt::where('id',$id)->first();
           
            PDF::writeHTML(view('webpage.printreceipt',compact('header'))->render());
            ob_end_clean();
            PDF::Output('Receipt.pdf','I');
        }
    }
    public function receipt(){
        $tab = ['tab' => 'Receipt', 'subtab' => 'index'];
        $headers = EnrollHeader::leftJoin('students','enroll_headers.student_id','=','students.id')
                ->select('enroll_headers.*','students.lname as lname','students.mname as mname','students.fname as fname')->orderBy('students.lname','asc')
                ->orderBy('created_at','asc')->paginate(10);
        if(Session::get('user_id')){
            return view('webpage/receipt', compact('tab','headers'));
        }
        else{
            Session::flush();
            return redirect()->action('WelcomeController@checklogin');
        }
    }

    public function savereceipt(){
        if(Input::get('addreceipt')){
            $new = new Receipt;
            $new->student_id = Input::get('studentid');
            $new->bill_id = Input::get('bill_id');
            $new->bill_date = date('Y-m-d', strtotime(Input::get('rdate')));
            $new->receipt_no = Input::get('receipt_no');
            $new->amount_paid = Input::get('amount_paid');
            $new->save();
            $bill = AcctHeader::where('id',$new->bill_id)->first();
            $bill->credit = $bill->credit +  $new->amount_paid;
            $bill->balance = $bill->balance - $new->amount_paid;
            $bill->save();

            Session::flash('alert-success', 'Receipt successfully receipt.');
            return redirect()->action('Info\InfoController@receipt');
        }
    }

    public function settings(){
        $tab = ['tab' => 'Settings', 'subtab' => 'index'];
       
        $subjects = Subject::where('status','Active')->orderBy('name')->orderBy('year')->orderBy('course_id')->get();
        $courses = Course::all();
        if(Session::get('user_id')){
            return view('webpage/settings', compact('tab','subjects','courses'));
        }
        else{
            Session::flush();
            return redirect()->action('WelcomeController@checklogin');
        }
    }

    public function savesettings(){
        if(Input::get('save')){
            $sub = new Subject;
            $sub->course_no = Input::get('course_no');
            $sub->name = Input::get('name');
            $sub->units = Input::get('units');
            $sub->year = Input::get('year');
            $sub->course_id = Input::get('course');
            $sub->save();

            Session::flash('alert-success', 'Subject successfully saved.');
            return redirect()->action('Info\InfoController@settings');
        }
        else if(Input::get('update')){
            $sub = Subject::where('id',Input::get('edit_id'))->first();
            $sub->course_no = Input::get('edit_course_no');
            $sub->name = Input::get('edit_name');
            $sub->units = Input::get('edit_units');
            $sub->year = Input::get('edit_year');
            $sub->course_id = Input::get('edit_course');
            $sub->save();

            Session::flash('alert-success', 'Subject successfully updated.');
            return redirect()->action('Info\InfoController@settings');
        }
    }

    public function studentlist(){
        $list = DB::select("select id, name from students");
        //$list = User::where('status','Active')->select('id','fname as name')->get();
        return Response::json($list, 200, array(), JSON_PRETTY_PRINT);
    }

    public function bills(){
        $id = Input::get('studentid');
        $bill = AcctHeader::where('student_id',$id)->first();
        return Response::json($bill, 200, array(), JSON_PRETTY_PRINT);
    }

    public function searchsubject(){
        $cid = Input::get('cid');
        $year = Input::get('year');
        $subjects = Subject::where('year',$year)->where('course_id',$cid)->get();
        return Response::json($subjects, 200, array(), JSON_PRETTY_PRINT);
    }

    public function gradeprofile($id){
        $tab = ['tab' => 'Accounting', 'subtab' => 'index'];
        $lists = Grade::leftJoin('subjects','grades.subject_id','=','subjects.id')
                        ->select('subjects.name as subject','grades.grade as grade','grades.id as id')
                        ->orderBy('subjects.id','asc')->get();
        if(Session::get('user_id')){
            return view('webpage/gradeprofile', compact('tab','lists'));
        }
        else{
            Session::flush();
            return redirect()->action('WelcomeController@checklogin');
        }
    }

    public function gradeupdate($id){
        if(Input::get('update')){
            $rows = count(Input::get('grade_id'));
            if($rows > 0){
                for($i=0;$i<$rows;$i++){
                    $up = Grade::where('id',Input::get('grade_id')[$i])->first();
                    $up->grade = Input::get('grade_gade')[$i];
                    $up->save();
                }
            }
            Session::flash('alert-success', 'Grade successfully updated.');
            return redirect()->action('Info\InfoController@gradeprofile', $id);
        }
        else if(Input::get('print')){
            PDF::SetTitle('Grades');
            PDF::SetFont('dejavusans', '', 10);
            PDF::SetMargins(15, 15, 0, true);
            PDF::AddPage();
            $student = Student::where('id',$id)->first();
            $grades = Grade::leftJoin('subjects','grades.subject_id','=','subjects.id')
                ->select('subjects.name as subject','grades.grade as grade')
                ->where('grades.student_id',$id)
                ->orderBy('subjects.id','asc')
                ->get();
            PDF::writeHTML(view('print.grade',compact('header','grades','student'))->render());
            ob_end_clean();
            PDF::Output('Grades.pdf','I');
        }
    }

    public function enrollprofile($id){
        $tab = ['tab' => 'Receipt', 'subtab' => 'index'];
        $courses = Course::all();
        $header = EnrollHeader::where('id',$id)->first();
        $details = EnrollDetail::where('header_id',$id)->get();
        if(Session::get('user_id')){
            return view('webpage/enrollprofile', compact('tab','courses','header','details'));
        }
        else{
            Session::flush();
            return redirect()->action('WelcomeController@checklogin');
        }
    }

    public function enrollupdate($id){
        $header = EnrollHeader::where('id',$id)->first();
        $header->student_id = Input::get('student_id');
        $header->semester = Input::get('semester');
        $header->sy1 = Input::get('sy1');
        $header->sy2 = Input::get('sy2');
        $header->birth_date = date('Y-m-d', strtotime(Input::get('birth_date')));
        $header->birth_place = Input::get('birth_place');
        $header->civil_status = Input::get('civil_status');
        $header->course_id = Input::get('course_id');
        $header->major = Input::get('major');
        $header->year = Input::get('year');
        $header->contact_number = Input::get('contact_number');
        $header->credential = Input::get('credential');
        $header->school_attended = Input::get('school_attended');
        $header->attended_school_year = Input::get('attended_school_year');
        $header->guardian = Input::get('guardian');
        $header->home_address = Input::get('home_address');
        $header->contact_no = Input::get('contact_no');
        $header->tabango_address = Input::get('tabango_address');
        $header->notify = Input::get('notify');
        $header->reg_date = date('Y-m-d', strtotime(Input::get('reg_date')));
        $header->save();

        $rows = count(Input::get('dtl_course_id'));
        if($rows > 0){
            for($i=0;$i<$rows;$i++){
                $dtl = EnrollDetail::where('id',Input::get('dtl_id')[$i])->first();
                $dtl->schedule = Input::get('dtl_schedule')[$i];
                $dtl->room_no = Input::get('dtl_room_no')[$i];
                $dtl->instructor = Input::get('dtl_instructor')[$i];
                $dtl->save();
            }
        }
        Session::flash('alert-success', 'Succesfully updated.');
        return redirect()->action('Info\InfoController@enrollprofile', $id);
    }

    public function enrollform(){
        $tab = ['tab' => 'Receipt', 'subtab' => 'index'];
        $courses = Course::all();
        if(Session::get('user_id')){
            return view('webpage/enrollform', compact('tab','courses'));
        }
        else{
            Session::flush();
            return redirect()->action('WelcomeController@checklogin');
        }
    }

    public function saveEnroll(){
        $header = new EnrollHeader;
        $header->student_id = Input::get('student_id');
        $header->semester = Input::get('semester');
        $header->sy1 = Input::get('sy1');
        $header->sy2 = Input::get('sy2');
        $header->birth_date = date('Y-m-d', strtotime(Input::get('birth_date')));
        $header->birth_place = Input::get('birth_place');
        $header->summer = Input::get('summer');
        $header->civil_status = Input::get('civil_status');
        $header->course_id = Input::get('course_id');
        $header->major = Input::get('major');
        $header->year = Input::get('year');
        $header->contact_number = Input::get('contact_number');
        $header->credential = Input::get('credential');
        $header->school_attended = Input::get('school_attended');
        $header->attended_school_year = Input::get('attended_school_year');
        $header->guardian = Input::get('guardian');
        $header->home_address = Input::get('home_address');
        $header->contact_no = Input::get('contact_no');
        $header->tabango_address = Input::get('tabango_address');
        $header->notify = Input::get('notify');
        $header->reg_date = date('Y-m-d', strtotime(Input::get('reg_date')));
        $header->save();

        $rows = count(Input::get('dtl_course_id'));
        if($rows > 0){
            for($i=0;$i<$rows;$i++){
                $dtl = new EnrollDetail;
                $dtl->header_id = $header->id;
                $dtl->course_no = Input::get('dtl_course_no')[$i];
                $dtl->course_id = Input::get('dtl_course_id')[$i];
                $dtl->units = Input::get('dtl_units')[$i];
                $dtl->schedule = Input::get('dtl_schedule')[$i];
                $dtl->room_no = Input::get('dtl_room_no')[$i];
                $dtl->instructor = Input::get('dtl_instructor')[$i];
                $dtl->save();
            }
        }
        
        Session::flash('alert-success', 'Succesfully enrolled.');
        return redirect()->action('Info\InfoController@enrollprofile', $header->id);
    }
}
