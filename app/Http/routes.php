<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Route::get('/', function () {
//     return view('auth/login');
// });

//Route::auth();
Route::get('/', 'WelcomeController@checklogin');
Route::post('/', 'WelcomeController@login');
Route::get('/logout', 'WelcomeController@logout');

Route::get('/home', 'HomeController@index');
Route::get('webpage/index','Info\InfoController@getIndex');
Route::post('webpage/index','Info\InfoController@postIndex');
Route::get('webpage/accounting','Info\InfoController@accounting');
Route::post('webpage/accounting','Info\InfoController@saveaccounting');
Route::get('webpage/gradeprofile={id}','Info\InfoController@gradeprofile');
Route::post('webpage/gradeprofile={id}','Info\InfoController@gradeupdate');

Route::get('webpage/receipt','Info\InfoController@receipt');
Route::post('webpage/receipt','Info\InfoController@savereceipt');
Route::get('webpage/receiptprofile={id}','Info\InfoController@receiptprofile');
Route::post('webpage/receiptprofile={id}','Info\InfoController@updateprofile');

Route::get('webpage/settings','Info\InfoController@settings');
Route::post('webpage/settings','Info\InfoController@savesettings');

Route::get('webpage/enrollform','Info\InfoController@enrollform');
Route::post('webpage/enrollform','Info\InfoController@saveEnroll');

Route::get('webpage/enrollprofile={id}','Info\InfoController@enrollprofile');
Route::post('webpage/enrollprofile={id}','Info\InfoController@enrollupdate');

Route::get('dev/api/studentlist','Info\InfoController@studentlist');
Route::get('dev/api/searchsubject','Info\InfoController@searchsubject');
