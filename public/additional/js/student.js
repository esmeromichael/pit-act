
$('.edit_info').on('click',function(){
	var id = $(this).data('id');
	var fname = $(this).data('fname');
	var mname = $(this).data('mname');
	var lname = $(this).data('lname');
	var course = $(this).data('courseid');
	var year = $(this).data('yearlevel');
	var address = $(this).data('address');
	var student_id = $(this).data('studentid');
	var code  = $(this).data('code');
	$('.edit_dtl_id').val('');
	$('.edit_fname').val('');
	$('.edit_mname').val('');
	$('.edit_lname').val('');
	$('.edit_address').val('');
	$('.edit_student_id').val('');
	$('.code').val('');

	$('.edit_dtl_id').val(id);
	$('.edit_student_id').val(student_id);
	$('.edit_course option').each(function(e){
		var ids = $(this).val();
		if(ids == course){
			$(this, 'option').attr('selected','selected')
		}
	});

	$('.edit_year option').each(function(e){
		var idss = $(this).val();
		if(idss == year){
			$(this, 'option').attr('selected','selected')
		}
	});

	$('.edit_fname').val(fname);
	$('.edit_mname').val(mname);
	$('.edit_lname').val(lname);
	$('.edit_address').val(address);
	$('.code').val(code);
}) 

function isNumberKey(evt, element){
    var charCode = (evt.which) ? evt.which : event.keyCode
    if ((charCode != 46 || $(element).val().indexOf('.') != -1) && // “.” CHECK DOT, AND ONLY ONE. 
        (charCode < 48 || charCode > 57))
        return false;
    return true;
}

function leftPad(num, length) {
  var result = '' + num;
  while (result.length < length) {
    result = '0' + result;
  }
  return result;
}

function pad (str, max) {
    str = str.toString();
    return str.length < max ? pad("0" + str, max) : str;
}

//=====================for purchases========================

//==========================================================

