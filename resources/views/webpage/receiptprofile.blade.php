@extends('layouts.header')
@section('content')
<style type="text/css">
    .inner-addon {
      position: relative;
    }

    /* style glyph */
    .inner-addon .glyphicon {
      position: absolute;
      padding: 10px;
      pointer-events: none;
      color:#336699;
      top:-7px;
    }
    /*tr,td{
        border:1px solid black;
    }*/
    /* align glyph */
    .right-addon .glyphicon { right: 0px;}

    /* add padding  */  
    .right-addon input { padding-right: 30px; }
</style>
  <section class="container main_section">
    <div class="form-group" style="margin-top:20px;">
        <div class="col-xs-11">
        <div class="flash-message">
            @foreach (['danger', 'warning', 'success', 'info'] as $message)
            @if(Session::has('alert-' . $message))
            <p class="alert alert-{{ $message }}" style="padding-top:5px;height:30px;">{{ Session::get('alert-' . $message) }}</p>
            @endif
            @endforeach
        </div>
        </div>
    </div>
    <div class="container" id="myTable">
        <form class="form-horizontal"  name="student" role="form"  method="POST" action="" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="form-group">
                <label class="col-md-3 control-label">Student Name</label>
                <div class="col-md-4">
                    <input type="text" class="form-control" value="{{$header->student->name}}" readonly>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label">Date</label>
                <div class="col-md-3">
                    <input type="text" class="form-control rdate" name="rdate" value="{{date('M-d-Y', strtotime($header->bill_date))}}" required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label">Receipt No.</label>
                <div class="col-md-3">
                    <input type="text" class="form-control" value="{{$header->receipt_no}}" name="receipt_no" required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label">Amount Paid</label>
                <div class="col-md-3">
                    <input type="text" class="form-control amount_paid" name="amount_paid" value="{{$header->amount_paid}}" required onkeypress="return isNumberKey(event, this);" style="text-align: right;">
                    <input type="hidden" class="form-control original_amount" name="original_amount" value="{{$header->amount_paid}}" >
                </div>
            </div>
            <br>
            <div class="form-group">
                <label class="col-md-3 control-label"></label>
                <button class="btn btn-lg btn-primary btn-sm update" id="myBtn" type="submit" name="update" value="update"><img src="/images/update1.png">&nbsp;Update</button>
                <button class="btn btn-lg btn-primary btn-sm print" id="myBtn" type="submit" name="print" value="print"><img src="/images/print1.png">&nbsp;Print</button>
            </div>
        </form>
    </div>
    <footer class="footer">
        <nav class="navbar navbar-inverse navbar-fixed-bottom">
            
        </nav>
    </footer>
    </section>
   <script type="text/javascript">
        function isNumberKey(evt, element){
            var charCode = (evt.which) ? evt.which : event.keyCode
            if ((charCode != 46 || $(element).val().indexOf('.') != -1) && // “.” CHECK DOT, AND ONLY ONE. 
                (charCode < 48 || charCode > 57))
                return false;
            return true;
        }

        $(".rdate").datepicker({
            dateFormat: "M-dd-yy",
            yearRange: "1950:2050",
            changeYear: true,
            changeMonth: true,
        });

        $('.amount_paid').on('change',function(e){
            var $this = $(this);
            $this.val(parseFloat($this.val()).toFixed(2));
        })

        $('.print').hover(function(){
            $('form').prop('target','_blank');
        }, function(){
            $('form').removeAttr('target');
        });
    </script>

@endsection
