@extends('layouts.header')
@section('content')
<div class="form-group" style="margin-top:20px;">
    <div class="col-xs-11">
        <div class="flash-message">
            @foreach (['danger', 'warning', 'success', 'info'] as $message)
            @if(Session::has('alert-' . $message))
            <p class="alert alert-{{ $message }}" style="padding-top:5px;height:30px;">{{ Session::get('alert-' . $message) }}</p>
            @endif
            @endforeach
        </div>
    </div>
</div>

    <div class="container" style="border-radius:3px;">
        <label class="col-xs-5"><h3>Subject</h3></label>
        <form class="form-horizontal"  name="employe" role="form"  method="POST" action="">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th width="8%">No</th>
                        <th width="30%">Subject</th>
                        <th width="15%">No.of Units</th>
                        <th width="20%">Year</th>
                        <th width="20%">Course</th>
                        <th width="10%">Status</th>
                    </tr>
                </thead>
                <tbody class="main">
                    @foreach($subjects as $sub)
                        <tr>
                            <td>
                                {{$sub->id}}
                            </td>
                            <td>
                                <a href="#" class="edit_misc" data-toggle="modal" data-target="#editSubject" data-id="{{$sub->id}}"
                                    data-name="{{$sub->name}}" data-year="{{$sub->year}}" data-course="{{$sub->course_id}}" data-units="{{$sub->units}}"
                                    data-courseno="{{$sub->course_no}}">
                                {{$sub->name}}
                                </a>
                            </td>
                            <td>
                               {{$sub->units}} 
                            </td>
                            <td>
                                {{$sub->year}}
                            </td>
                            <td>
                                {{$sub->course->short}}
                            </td>
                            <td>
                                {{$sub->status}}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            
        </form>
    </div>
    <div class="modal fade" id="addSubject" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content" >
                <div class="modal-header">
                    <h4><b>New Subject</b></h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" id="subs" role="form"  method="POST" action="">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <label class="control-label col-xs-4">Course No.</label> 
                            <div class="col-xs-7">
                                <input type="text" class="form-control course_no" value="" name="course_no" required>
                            </div>   
                        </div>
                        <div class="form-group">
                            <label class="control-label col-xs-4">Descripted Title</label> 
                            <div class="col-xs-7">
                                <input type="text" class="form-control name" value="" name="name" required>
                            </div>   
                        </div>
                        <div class="form-group">
                            <label class="control-label col-xs-4">No. of Unts</label> 
                            <div class="col-xs-7">
                                <input type="text" class="form-control units" value="" name="units" required>
                            </div>   
                        </div>
                        <div class="form-group">
                            <label class="control-label col-xs-4">Year</label> 
                            <div class="col-xs-7">
                                <select class="form-control" name="year" required="">
                                    <option value="" selected="">--Select--</option>
                                    <option value="First">First</option>
                                    <option value="Second">Second</option>
                                    <option value="Third">Third</option>
                                    <option value="Fourth">Fourth</option>
                                </select>
                            </div>   
                        </div>
                        <div class="form-group">
                            <label class="control-label col-xs-4">Course</label> 
                            <div class="col-xs-7">
                                <select class="form-control" name="course" required="">
                                    <option value="" selected="">--Select--</option>
                                    @foreach($courses as $course)
                                        <option value="{{$course->id}}">{{$course->short}}</option>
                                    @endforeach
                                </select>
                            </div>   
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-lg btn-primary btn-sm" form="subs" id="myBtn" name="save" value="Save"><img src="/images/save1.png">&nbsp;Save</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="editSubject" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content" >
                <div class="modal-header">
                    <h4><b>Update Subject</b></h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" id="subsedit" role="form"  method="POST" action="">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <label class="control-label col-xs-4">Course No.</label> 
                            <div class="col-xs-7">
                                <input type="hidden" class="form-control edit_id" value="" name="edit_id">
                                <input type="text" class="form-control edit_course_no" value="" name="edit_course_no" required>
                            </div>   
                        </div>
                        <div class="form-group">
                            <label class="control-label col-xs-4">Descripted Title</label> 
                            <div class="col-xs-7">
                                <input type="text" class="form-control edit_name" value="" name="edit_name" required>
                            </div>   
                        </div>
                        <div class="form-group">
                            <label class="control-label col-xs-4">No. of Unts</label> 
                            <div class="col-xs-7">
                                <input type="text" class="form-control edit_units" value="" name="edit_units" required>
                            </div>   
                        </div>
                        <div class="form-group">
                            <label class="control-label col-xs-4">Year</label> 
                            <div class="col-xs-7">
                                <select class="form-control edit_year" name="edit_year" required="">
                                    <option value="First">First</option>
                                    <option value="Second">Second</option>
                                    <option value="Third">Third</option>
                                    <option value="Fourth">Fourth</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-xs-4">Course</label> 
                            <div class="col-xs-7">
                                <select class="form-control edit_course" name="edit_course" required="">
                                    @foreach($courses as $course)
                                        <option value="{{$course->id}}">{{$course->short}}</option>
                                    @endforeach
                                </select>
                            </div>   
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-lg btn-primary btn-sm" form="subsedit" id="myBtn" name="update" value="Update"><img src="/images/update1.png">&nbsp;Update</button>
                </div>
            </div>
        </div>
    </div>

    </section>
    <script type="text/javascript">
    function isNumberKey(evt, element){
        var charCode = (evt.which) ? evt.which : event.keyCode
        if ((charCode != 46 || $(element).val().indexOf('.') != -1) && // “.” CHECK DOT, AND ONLY ONE. 
            (charCode < 48 || charCode > 57))
            return false;
        return true;
    }

    $('.amount').on('change',function(){
        var $this = $(this);
        $this.val(parseFloat($this.val()).toFixed(2));
    })

    $('.edit_misc').on('click',function(){
        var id = $(this).data('id');
        var name = $(this).data('name');
        var year = $(this).data('year');
        var course = $(this).data('course');
        var units = $(this).data('units');
        var courseno = $(this).data('courseno');
        $('.edit_id').val('');
        $('.edit_name').val('');
        $('.edit_course_no').val('');
        $('.edit_id').val(id);
        $('.edit_course_no').val(courseno);
        $('.edit_name').val(name);
        $('edit_units').val(units)
        $('#edit_year option[value="'+year+'"]').attr('selected', 'selected');
        $('#edit_course option[value="'+course+'"]').attr('selected', 'selected');
    });

</script>
    
@endsection
