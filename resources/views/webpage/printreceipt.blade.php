<!DOCTYPE html>
<html>
<head>
	<title>Billing Statement</title>
	<style type="text/css">
		.circle {
			font-size: 8pt;
			text-align: left;
		}
		.circle1 {
			font-size: 8pt;
		}
		.circle2 {
			text-align: right;
			font-size: 8pt;
		}
		
		.left{
			width:75%;
			border: 0.3px solid #333;
		}
		.right{
			width:25%;
			text-align: center;
			border: 0.3px solid #333;
		}
		.border{
			border: 0.3px solid #333;
		}
		.barHeight{
			height: 100px;
		}
	</style>
</head>
<body>
<table style="width: 65%;border: 1px solid #333;">
	<tbody>
		<tr style="">
			<td>
				<div class="" style="text-align: center;">
					<br>Republic of the Philippines<br>
					<label style="font-size:9px;">PALOMPON INSTITUTE OF TECHNOLOGY <br>Tabango Campus
					</label><br>
				</div>
			</td>
		</tr>
		<tr style="font-size:8pt;">
			<td width="1.5%"></td>
			
			<td style="width:100px;text-align:right;">
				<label style="text-align:left;">Name</label>
			</td>
			<td style="width:100px;">
				<label style="text-align:left;">{{$header->student->name}}</label>
			</td>
		</tr>
		<tr style="font-size:8pt;">
			<td width="1.5%"></td>
			
			<td style="width:100px;text-align:right;">
				<label style="text-align:left;">Date:</label>
			</td>
			<td style="width:100px;">
				<label style="text-align:left;">{{date('M-d-Y', strtotime($header->bill_date))}}</label>
			</td>
		</tr>
		<tr style="font-size:8pt;">
			<td width="1.5%"></td>
			
			<td style="width:100px;text-align:right;">
				<label style="text-align:left;">Amount Paid:</label>
			</td>
			<td style="width:100px;">
				<label style="text-align:left;">{{$header->amount_paid}}</label>
			</td>
		</tr>
	</tbody>
</table> 
 
</body> 
</html>

