@extends('layouts.header')
@section('content')
<style type="text/css">
  /*th,td{
    border:1px solid black;
  }*/
   /* enable absolute positioning */
    .inner-addon {
      position: relative;
    }

    /* style glyph */
    .inner-addon .glyphicon {
      position: absolute;
      padding: 10px;
      pointer-events: none;
      color:#336699;
      top:-7px;
    }

    /* align glyph */
    .right-addon .glyphicon { right: 0px;}

    /* add padding  */  
    .right-addon input { padding-right: 30px; }
</style>
  <section class="container main_section">
    <div class="form-group" style="margin-top:20px;">
        <div class="col-xs-11">
            <div class="flash-message">
                @foreach (['danger', 'warning', 'success', 'info'] as $message)
                @if(Session::has('alert-' . $message))
                <p class="alert alert-{{ $message }}" style="padding-top:5px;height:30px;">{{ Session::get('alert-' . $message) }}</p>
                @endif
                @endforeach
            </div>
        </div>
    </div>
    <table class="table table-striped table-hover s_table">
    <thead>
        <tr>
            <th width="10%">Student ID</th>
            <th width="40%">Name</th>
            <th width="15%">Course</th>
            <th width="10%">Year</th>
            <th width="30%">Gender</th>
            <th width="15%">Code</th>
        </tr>
    </thead>
    <tbody class="s_tbody">
        @foreach($students as $student)
            <tr>
                <td>
                <a href="#" class="edit_info" data-type="appointment" data-toggle="modal" data-target="#editstudent" 
                    data-studentid="{{$student->student_id}}" data-courseid="{{$student->course_id}}" data-yearlevel="{{$student->year_level}}"
                    data-fname="{{$student->fname}}" data-mname="{{$student->mname}}" data-lname="{{$student->lname}}" data-address="{{$student->address}}"
                    data-id="{{$student->id}}" data-code="{{$student->code}}">{{$student->student_id}}</a>
                </td>
                <td>{{$student->lname}}, {{$student->fname}}  {{$student->mname}}</td>
                <td>{{$student->course->short}}</td>
                <td>{{$student->year_level}}</td>
                <td>{{$student->address}}</td>
                <td>{{$student->code}}</td>
            </tr>
        @endforeach
    </tbody>
    <tfoot class="s_tfoot">
        <div id="page-selection" class="pagination" style=" position:fixed; bottom: 15px; width: 700px;margin-bottom:10px;margin-left:-1000px;">
           {!! with(new Illuminate\Pagination\BootstrapThreePresenter($students))->render()!!}
        </div>
    </tfoot> 
    <div class="loading"></div>
</table>
    <div class="modal fade" id="myModal1" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content" >
                <div class="modal-header">

                    <h4><b>New Student</b></h4>
                </div>

                <div class="modal-body">
                <h6>Create New Student Info</h6>
                 <form class="form-horizontal"  name="student" role="form"  method="POST" action="" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Student ID</label>
                        <div class="col-md-5">
                            <input type="text" class="student_id form-control" name="student_id" value="" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Course</label>
                        <div class="col-md-5">
                            <select class="form-control" name="course" required="">
                                @foreach($courses as $course)
                                    <option value="{{$course->id}}">{{$course->short}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Year</label>
                        <div class="col-md-5">
                            <select class="form-control" name="year" required="">
                                <option value="First">First</option>
                                <option value="Second">Second</option>
                                <option value="Third">Third</option>
                                <option value="Fourth">Fourth</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">First Name</label>
                        <div class="col-md-5">
                            <input type="text" class="form-control" name="fname" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Middle Name</label>
                        <div class="col-md-5">
                            <input type="text" class="form-control" name="mname" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Last Name</label>
                        <div class="col-md-5">
                            <input type="text" class="form-control" name="lname" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Address</label>
                        <div class="col-md-7">
                            <input type="text" class="form-control" name="address" >
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-lg btn-primary btn-sm" id="myBtn" type="submit" name="addstudent" value="addstudent"><img src="/images/save1.png">&nbsp;Save</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="editstudent" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content" >
                <div class="modal-header">
                    <h4><b>Edit Student</b></h4>
                </div>
                <div class="modal-body">
                <h6>Edit Student Info</h6>
                 <form class="form-horizontal"  name="editstudent" role="form"  method="POST" action="" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Student ID</label>
                        <div class="col-md-5"><input type="hidden" class="edit_dtl_id form-control" name="edit_dtl_id" value="">
                            <input type="text" class="edit_student_id form-control" name="edit_student_id" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Course</label>
                        <div class="col-md-5">
                            <select class="form-control edit_course" name="edit_course">
                                @foreach($courses as $course)
                                    <option value="{{$course->id}}">{{$course->short}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Year</label>
                        <div class="col-md-5">
                            <select class="form-control edit_year" name="edit_year">
                                <option value="First">First</option>
                                <option value="Second">Second</option>
                                <option value="Third">Third</option>
                                <option value="Fourth">Fourth</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">First Name</label>
                        <div class="col-md-5">
                            <input type="text" class="form-control edit_fname" name="edit_fname" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Middle Name</label>
                        <div class="col-md-5">
                            <input type="text" class="form-control edit_mname" name="edit_mname" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Last Name</label>
                        <div class="col-md-5">
                            <input type="text" class="form-control edit_lname" name="edit_lname" >
                        </div>
                    </div>
                    
                   
                    <div class="form-group">
                        <label class="col-md-3 control-label">Address</label>
                        <div class="col-md-5">
                            <input type="text" class="form-control edit_address" name="edit_address" >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label">Code</label>
                        <div class="col-md-5">
                            <input type="text" class="form-control code" value="" readonly="">
                        </div>
                    </div>
                    <div class="modal-footer">
                    <button class="btn btn-lg btn-primary btn-sm" id="myBtn" type="submit" name="edit_student" value="edit_student"><img src="/images/update1.png">Update</button>
                    </div>
                    </form>
                </div>
            </div> 
        </div>
    </div>
    <div id="studentSearch" class="modal fade" role="dialog">
        <div class="modal-dialog" style="width:40%;">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Search</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" role="form"  method="POST" action="" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group" style="display:none;">
                            <label class="contorl-label col-sm-3">Doc ID :</label>
                            <div class="col-sm-4">
                                <div class="inner-addon right-addon">
                                    <i class="glyphicon glyphicon-search"></i>
                                    <input type="text" class="form-control s_student_id" id="s_student_id" name="s_student_id" value="" maxlength="6" tabindex="1" autofocus onkeypress="return isNumberKey(event, this);">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="contorl-label col-sm-3">Student ID :</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control student_schoolid" id="student_schoolid" name="student_schoolid" value="" tabindex="-1">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="contorl-label col-sm-3">Name :</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control studentname searchstud" id="studentname" name="studentname" tabindex="-1">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default s_search" data-dismiss="modal"><img src="/images/search.png">Search</button>
                </div>
            </div>
        </div>
    </div>
    </section>
    <script type="text/javascript" src="{{ asset('/additional/js/student.js') }}"></script>
    <script type="text/javascript">
        $('.searchstud').typeahead({ 
            source:  function (query, process){
            return $.getJSON('/dev/api/searchsname?name='+query, function(data){
                return process(data);
            })
            }
        });
    </script>
@endsection
