@extends('layouts.header')
@section('content')
<style type="text/css">
 
</style>
  <section class="container main_section">
    <div class="form-group" style="margin-top:20px;">
        <div class="col-xs-11">
        <div class="flash-message">
            @foreach (['danger', 'warning', 'success', 'info'] as $message)
            @if(Session::has('alert-' . $message))
            <p class="alert alert-{{ $message }}" style="padding-top:5px;height:30px;">{{ Session::get('alert-' . $message) }}</p>
            @endif
            @endforeach
        </div>
        </div>
    </div>
    <div class="row">
        <form class="form-horizontal"  name="enroll" id="enroll" role="form"  method="POST" action="" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                    <label class="control-label col-sm-2">Partner Name :</label>
                    <div class="col-sm-3">
                        <select class="studentid form-control"  data-width="100%" id="student_id" name="student_id" required>
                            <option value="{{$header->student_id}}" selected="selected">{{$header->student->name}}</option>
                        </select>
                    </div>
                    <label class="control-label col-sm-1">Semester</label>
                    <div class="col-sm-2">
                        <select class="form-control" name="semester" required="">
                            <option value="{{$header->semester}}" selected="">{{$header->semester}}</option>
                            @if($header->semester == "First")
                            <option value="Second">Second</option>
                            @else
                            <option value="First">First</option>
                            @endif
                        </select>
                    </div>
                    <label class="control-label col-sm-1">SY:</label>
                    <div class="col-xs-3">
                        <input type="text" class="form-control sy1" id="sy1" name="sy1" value="{{$header->sy1}}" style="width: 40%;display: inline-block;" required="">
                        <input type="text" class="form-control sy2" id="sy2" name="sy2" value="{{$header->sy2}}" style="width: 40%;display: inline-block;" required="">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2">Date of Birth :</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control birth_date" name="birth_date" value="{{date('M-d-Y', strtotime($header->birth_date))}}" required="">
                    </div>
                    <label class="control-label col-sm-2">Place of Birth :</label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control birth_place" name="birth_place" value="{{$header->birth_place}}">
                    </div>
                    <label class="control-label col-sm-1">Summer :</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control summer" name="summer" value="{{$header->summer}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2">Civil Status :</label>
                    <div class="col-sm-2">
                        <input type="hidden" class="default_civil_status" value="{{$header->civil_status}}">
                        <select class="form-control civil_status" id="civil_status" name="civil_status" required="">
                            <option value="Single">Single</option>
                            <option value="Married">Married</option>
                            <option value="Separated">Separated</option>
                            <option value="Widow">Widow</option>
                        </select>
                    </div>
                    <label class="control-label col-sm-1">Course :</label>
                    <div class="col-sm-4">
                        <input type="hidden" class="default_course" value="{{$header->course_id}}">
                        <select class="form-control course" id="course" name="course_id" required="">
                            <option value="" selected="">--Select--</option>
                            @foreach($courses as $course)
                                <option value="{{$course->id}}">{{$course->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <label class="control-label col-sm-1">Major :</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control major" name="major" value="{{$header->major}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2">Year :</label>
                    <div class="col-sm-2">
                        <input type="hidden" class="default_year" value="{{$header->year}}">
                        <select class="form-control year" id="year" name="year" required="">
                            <option value="" selected="">--Select--</option>
                            <option value="First">First</option>
                            <option value="Second">Second</option>
                            <option value="Third">Third</option>
                            <option value="Fourth">Fourth</option>
                        </select>
                    </div>
                    <label class="control-label col-sm-2">Contact Number :</label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control contact_number" name="contact_number" value="{{$header->contact_number}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2">Credential Submitted :</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control credential" name="credential" value="{{$header->credential}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2">School Last Attended :</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control school_attended" name="school_attended" value="{{$header->school_attended}}">
                    </div>
                    <label class="control-label col-sm-2">School Year :</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control attended_school_year" name="attended_school_year" value="{{$header->attended_school_year}}">
                    </div>
                </div>
                <hr>
                <div class="form-group">
                    <label class="control-label col-sm-2">Parent / Guardian :</label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control guardian" name="guardian" value="{{$header->gurdian}}">
                    </div>
                    <label class="control-label col-sm-2">Home Address :</label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control home_address" name="home_address" value="{{$header->home_address}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2">Contact No. :</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control contact_no" name="contact_no" value="{{$header->contact_no}}">
                    </div>
                    <label class="control-label col-sm-3">Tabango Address :</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control tabango_address" name="tabango_address" value="{{$header->tabango_address}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3">In case of emergency pls. notify :</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control notify" name="notify" value="{{$header->notify}}">
                    </div>
                    <label class="control-label col-sm-2">Date of Registration :</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control reg_date" name="reg_date" value="{{date('M-d-Y', strtotime($header->reg_date))}}">
                    </div>
                </div>
                <hr>
                <table class="table table-hover table-striped">
                    <thead>
                        <tr>
                            <th width="3%"></th>
                            <th width="10%">Course No.</th>
                            <th width="37%">Descriptive Title</th>
                            <th width="10%">No. of Units</th>
                            <th width="15%">Schedule</th>
                            <th width="10%">Room No.</th>
                            <th width="15%">Subject Instructor</th>
                        </tr>
                    </thead>
                    <tbody class="subject">
                        @foreach($details as $dtl)
                            <tr>
                            <td>
                                <input type="checkbox" class="check" value="" checked="">
                                <input type="hidden" class="dtl_id" name="dtl_id[]" value="{{$dtl->id}}">
                            </td>
                            <td>{{$dtl->course_no}}
                                <input type="hidden" class="dtl_course_no" name="dtl_course_no[]" value="{{$dtl->course_no}}">
                            </td>
                            <td>
                                {{$dtl->subject->name}}
                                <input type="hidden" class="dtl_course_id" name="dtl_course_id[]" value="{{$dtl->course_id}}">
                            </td>
                            <td>{{$dtl->units}}
                                <input type="hidden" class="dtl_units units_num" value="{{$dtl->units}}" name="dtl_units[]" style="text-align: center;">
                            </td>
                            <td><input type="text" class="form-control dtl_schedule" name="dtl_schedule[]" value="{{$dtl->schedule}}"></td>
                            <td><input type="text" class="form-control dtl_room_no" name="dtl_room_no[]" value="{{$dtl->room_no}}"></td>
                            <td><input type="text" class="form-control dtl_instructor" name="dtl_instructor[]" value="{{$dtl->instructor}}"></td>
                        </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <td></td>
                            <td></td>
                            <td style="text-align: right;">Total :</td>
                            <td style="text-align: center"><label class="total_units">0.00</label></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </tfoot>
                </table>
            <div class="form-group">
                <label class="form-control col-sm-2"> </label>
                <button class="btn btn-lg btn-primary btn-sm enroll" id="myBtn" type="submit" name="update" value="update"><img src="/images/update1.png">&nbsp;Update</button>
            </div>
        </form>
    </div>
    </section>
    <script type="text/javascript">
        $(document).ready(function(){
            var civil = $('.default_civil_status').val();
            var course = $('.default_course').val(); 
            var year = $('.default_year').val();
            $('#civil_status option[value="'+civil+'"]').attr('selected', 'selected');
            $('#course option[value="'+course+'"]').attr('selected', 'selected');
            $('#year option[value="'+year+'"]').attr('selected', 'selected');
            totalUnits();
        });

        $(".birth_date").datepicker({ 
            dateFormat: "yy",
            yearRange: "1950:2050",
            changeYear: true,
            changeMonth: true,
        });
        $(".reg_date").datepicker({ 
            dateFormat: "yy",
            yearRange: "1950:2050",
            changeYear: true,
            changeMonth: true,
        });

        $.getJSON('/dev/api/studentlist', function(data){
            data = $.map(data, function(partner){
            return {id: partner.id, text: partner.name };
            });
            $(".studentid").select2({
                minimumInputLength: 1,
                multiple: false,
                // placeholder: "Customer",  
                data: data 
            });
        });

        function isNumberKey(evt, element){
            var charCode = (evt.which) ? evt.which : event.keyCode
            if ((charCode != 46 || $(element).val().indexOf('.') != -1) && // “.” CHECK DOT, AND ONLY ONE. 
                (charCode < 48 || charCode > 57))
                return false;
            return true;
        }


        function totalUnits(){ 
            var headertotal = 0;
            $('.units_num').each(function(){ 
                var price = $(this).val();
                headertotal += parseFloat(price);
            });
            $('.total_units').text((headertotal).toFixed(2));
        }

        $(function() {
            var availableTags = [
            '2010',
            '2011',
            '2012',
            '2013',
            '2014',
            '2015',
            '2016',
            '2017',
            '2018',
            '2019',
            '2020',
            '2021',
            '2022',
            '2023',
            '2024',
            '2025',
            '2026',
            '2027',
            '2028',
            '2029',
            '2030',
            '2031',
            '2032',
            '2033',
            '2034',
            '2035',
            '2036',
            '2037',
            '2038',
            '2039',
            '2040'
            ];
            $( "#sy1, #sy2" ).autocomplete({
              source: availableTags
            });
        });
    </script>
@endsection
