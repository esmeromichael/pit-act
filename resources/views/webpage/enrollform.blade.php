@extends('layouts.header')
@section('content')
<style type="text/css">
 
</style>
  <section class="container main_section">
    <div class="form-group" style="margin-top:20px;">
        <div class="col-xs-11">
        <div class="flash-message">
            @foreach (['danger', 'warning', 'success', 'info'] as $message)
            @if(Session::has('alert-' . $message))
            <p class="alert alert-{{ $message }}" style="padding-top:5px;height:30px;">{{ Session::get('alert-' . $message) }}</p>
            @endif
            @endforeach
        </div>
        </div>
    </div>
    <div class="row">
        <form class="form-horizontal"  name="enroll" id="enroll" role="form"  method="POST" action="" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                    <label class="control-label col-sm-2">Partner Name :</label>
                    <div class="col-sm-3">
                        <select class="studentid form-control"  data-width="100%" id="student_id" name="student_id" required>
                            <option value="" selected="selected"></option>
                        </select>
                    </div>
                    <label class="control-label col-sm-1">Semester</label>
                    <div class="col-sm-2">
                        <select class="form-control" name="semester" required="">
                            <option value="" selected="">--Select--</option>
                            <option value="First">First</option>
                            <option value="Second">Second</option>
                        </select>
                    </div>
                    <label class="control-label col-sm-1">SY:</label>
                    <div class="col-xs-3">
                        <input type="text" class="form-control sy1" id="sy1" name="sy1" value="" style="width: 40%;display: inline-block;" required="">
                        <input type="text" class="form-control sy2" id="sy2" name="sy2" value="" style="width: 40%;display: inline-block;" required="">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2">Date of Birth :</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control birth_date" name="birth_date" value="" required="">
                    </div>
                    <label class="control-label col-sm-2">Place of Birth :</label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control birth_place" name="birth_place" value="">
                    </div>
                    <label class="control-label col-sm-1">Summer :</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control summer" name="summer" value="">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2">Civil Status :</label>
                    <div class="col-sm-2">
                        <select class="form-control civil_status" name="civil_status" required="">
                            <option value="Single" selected="">Single</option>
                            <option value="Married">Married</option>
                            <option value="Separated">Separated</option>
                            <option value="Widow">Widow</option>
                        </select>
                    </div>
                    <label class="control-label col-sm-1">Course :</label>
                    <div class="col-sm-4">
                        <select class="form-control course" name="course_id" required="">
                            <option value="" selected="">--Select--</option>
                            @foreach($courses as $course)
                                <option value="{{$course->id}}">{{$course->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <label class="control-label col-sm-1">Major :</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control major" name="major" value="">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2">Year :</label>
                    <div class="col-sm-2">
                        <select class="form-control year" name="year" required="">
                            <option value="" selected="">--Select--</option>
                            <option value="First">First</option>
                            <option value="Second">Second</option>
                            <option value="Third">Third</option>
                            <option value="Fourth">Fourth</option>
                        </select>
                    </div>
                    <label class="control-label col-sm-2">Contact Number :</label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control contact_number" name="contact_number" value="">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2">Credential Submitted :</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control credential" name="credential" value="">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2">School Last Attended :</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control school_attended" name="school_attended" value="">
                    </div>
                    <label class="control-label col-sm-2">School Year :</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control attended_school_year" name="attended_school_year" value="">
                    </div>
                </div>
                <hr>
                <div class="form-group">
                    <label class="control-label col-sm-2">Parent / Guardian :</label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control guardian" name="guardian" value="">
                    </div>
                    <label class="control-label col-sm-2">Home Address :</label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control home_address" name="home_address" value="">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2">Contact No. :</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control contact_no" name="contact_no" value="">
                    </div>
                    <label class="control-label col-sm-3">Tabango Address :</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control tabango_address" name="tabango_address" value="">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3">In case of emergency pls. notify :</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control notify" name="notify" value="">
                    </div>
                    <label class="control-label col-sm-2">Date of Registration :</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control reg_date" name="reg_date" value="">
                    </div>
                </div>
                <hr>
                <table class="table table-hover table-striped">
                    <thead>
                        <tr>
                            <th width="3%"></th>
                            <th width="10%">Course No.</th>
                            <th width="37%">Descriptive Title</th>
                            <th width="10%">No. of Units</th>
                            <th width="15%">Schedule</th>
                            <th width="10%">Room No.</th>
                            <th width="15%">Subject Instructor</th>
                        </tr>
                    </thead>
                    <tbody class="subject">
                        
                    </tbody>
                    <tfoot>
                        <tr>
                            <td></td>
                            <td></td>
                            <td style="text-align: right;">Total :</td>
                            <td style="text-align: center"><label class="total_units">0.00</label></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </tfoot>
                </table>
            <div class="form-group">
                <label class="form-control col-sm-2"> </label>
                <button class="btn btn-lg btn-primary btn-sm enroll" id="myBtn" type="submit" name="enroll" value="enroll"><img src="/images/save1.png">&nbsp;Save</button>
            </div>
        </form>
    </div>
    </section>
    
    <script type="text/javascript">
        $(".birth_date").datepicker({ 
            dateFormat: "yy",
            yearRange: "1950:2050",
            changeYear: true,
            changeMonth: true,
        });
        $(".reg_date").datepicker({ 
            dateFormat: "yy",
            yearRange: "1950:2050",
            changeYear: true,
            changeMonth: true,
        });
        function subjctLists(){
            var year = $('.year option:selected').val();
            var course_id = $('.course option:selected').val();
            $('table tbody.subject').empty();
            $.get('/dev/api/searchsubject?cid='+course_id+'&year='+year, function(data){
                $.each(data, function(index, dtl){
                    $('table tbody.subject').append('<tr>\
                            <td><input type="checkbox" class="check" value=""></td>\
                            <td>'+dtl.course_no+'\
                                <input type="hidden" class="dtl_course_no" value="'+dtl.course_no+'">\
                            </td>\
                            <td>'+dtl.name+'\
                                <input type="hidden" class="dtl_course_id" value="'+dtl.course_id+'">\
                            </td>\
                            <td>'+dtl.units+'\
                                <input type="hidden" class="dtl_units" value="'+dtl.units+'" style="text-align: center;">\
                            </td>\
                            <td><input type="text" class="form-control dtl_schedule" value=""></td>\
                            <td><input type="text" class="form-control dtl_room_no" value=""></td>\
                            <td><input type="text" class="form-control dtl_instructor" value=""></td>\
                        </tr>');
                });
                dtlCheck();
            });
        }
        $('.year').on('change',function(){
            subjctLists();
        })
         $('.course').on('change',function(){
            subjctLists();
        })
        function dtlCheck(){
            $('table tbody.subject').on('click','.check',function(){
                if($(this).is(':checked')){
                    $(this).closest('tr').find('.dtl_course_no').attr('name', 'dtl_course_no[]');
                    $(this).closest('tr').find('.dtl_course_id').attr('name', 'dtl_course_id[]');
                    $(this).closest('tr').find('.dtl_units').attr('name', 'dtl_units[]');
                    $(this).closest('tr').find('.dtl_schedule').attr('name', 'dtl_schedule[]');
                    $(this).closest('tr').find('.dtl_room_no').attr('name', 'dtl_room_no[]');
                    $(this).closest('tr').find('.dtl_instructor').attr('name', 'dtl_instructor[]');
                    $(this).closest('tr').find('.dtl_units').addClass('units_num');
                }
                else{
                    $(this).closest('tr').find('.dtl_course_no').removeAttr('name');
                    $(this).closest('tr').find('.dtl_course_id').removeAttr('name');
                    $(this).closest('tr').find('.dtl_units').removeAttr('name');
                    $(this).closest('tr').find('.dtl_schedule').removeAttr('name');
                    $(this).closest('tr').find('.dtl_room_no').removeAttr('name');
                    $(this).closest('tr').find('.dtl_instructor').removeAttr('name');
                    $(this).closest('tr').find('.dtl_units').removeAttr('name');
                    $(this).closest('tr').find('.dtl_units').removeClass('units_num');
                }
                totalUnits();
            })
        }

        function totalUnits(){ 
            var headertotal = 0;
            $('.units_num').each(function(){ 
                var price = $(this).val();
                headertotal += parseFloat(price);
            });
            $('.total_units').text((headertotal).toFixed(2));
        }
        $('.enroll').on('click',function(){
            var x = $('table tbody.subject tr').length;
            var u = $('.check:checked').length;
            if(x > 0){
                if(u > 0){
                    return true;
                }
                else{
                    alert('Please select subject')
                    return false;
                }
            }
            else{
                alert('Please add subject.')
                return false;
            }
        });

        $.getJSON('/dev/api/studentlist', function(data){
            data = $.map(data, function(partner){
            return {id: partner.id, text: partner.name };
            });
            $(".studentid").select2({
                minimumInputLength: 1,
                multiple: false,
                // placeholder: "Customer",  
                data: data 
            });
        });

        function isNumberKey(evt, element){
            var charCode = (evt.which) ? evt.which : event.keyCode
            if ((charCode != 46 || $(element).val().indexOf('.') != -1) && // “.” CHECK DOT, AND ONLY ONE. 
                (charCode < 48 || charCode > 57))
                return false;
            return true;
        }

        $(function() {
            var availableTags = [

            '2010',
            '2011',
            '2012',
            '2013',
            '2014',
            '2015',
            '2016',
            '2017',
            '2018',
            '2019',
            '2020',
            '2021',
            '2022',
            '2023',
            '2024',
            '2025',
            '2026',
            '2027',
            '2028',
            '2029',
            '2030',
            '2031',
            '2032',
            '2033',
            '2034',
            '2035',
            '2036',
            '2037',
            '2038',
            '2039',
            '2040'
            ];
            $( "#sy1, #sy2" ).autocomplete({
              source: availableTags
            });
        });
    </script>
@endsection
