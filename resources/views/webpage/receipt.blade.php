@extends('layouts.header')
@section('content')
<style type="text/css">
    .inner-addon {
      position: relative;
    }

    /* style glyph */
    .inner-addon .glyphicon {
      position: absolute;
      padding: 10px;
      pointer-events: none;
      color:#336699;
      top:-7px;
    }
    /*tr,td{
        border:1px solid black;
    }*/
    /* align glyph */
    .right-addon .glyphicon { right: 0px;}

    /* add padding  */  
    .right-addon input { padding-right: 30px; }
</style>
  <section class="container main_section">
    <div class="form-group" style="margin-top:20px;">
        <div class="col-xs-11">
        <div class="flash-message">
            @foreach (['danger', 'warning', 'success', 'info'] as $message)
            @if(Session::has('alert-' . $message))
            <p class="alert alert-{{ $message }}" style="padding-top:5px;height:30px;">{{ Session::get('alert-' . $message) }}</p>
            @endif
            @endforeach
        </div>
        </div>
    </div>
    <div class="col-xs-9" id="myTable">
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th width="8%">No</th>
                    <th width="30%">Student Name</th>
                    <th width="10%">Course</th>
                    <th width="10%">Year</th>
                </tr>
            </thead>
            <tbody class="e_tbody">
                @foreach($headers as $head)
                    <tr>
                        <td>
                            <?php $value = sprintf( '%08d', $head->id ); ?>
                            <a href="/webpage/enrollprofile={{$head->id}}">{{$value}}</a>
                        </td>
                        <td>
                            {{$head->lname}}, {{$head->fname}} {{$head->mname}}
                        </td>
                        <td>
                            {{$head->course->name}}
                        </td>
                        <td>{{$head->year}}</td>
                    </tr>
                @endforeach
            </tbody>
            <tfoot class="s_tfoot">
                <div id="page-selection" class="pagination" style=" position:fixed; bottom: 15px; width: 700px;margin-bottom:10px;margin-left:-1000px;">
                </div>
            </tfoot> 
            <div class="loading"></div>
        </table>
    </div>
    </section>
    <script type="text/javascript">
    </script>
@endsection
