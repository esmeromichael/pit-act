@extends('layouts.header')
@section('content')
<style type="text/css">
    .inner-addon {
      position: relative;
    }

    /* style glyph */
    .inner-addon .glyphicon {
      position: absolute;
      padding: 10px;
      pointer-events: none;
      color:#336699;
      top:-7px;
    }
    /*tr,td{
        border:1px solid black;
    }*/
    /* align glyph */
    .right-addon .glyphicon { right: 0px;}

    /* add padding  */  
    .right-addon input { padding-right: 30px; }
</style>
  <section class="container main_section">
    <div class="form-group" style="margin-top:20px;">
        <div class="col-xs-11">
        <div class="flash-message">
            @foreach (['danger', 'warning', 'success', 'info'] as $message)
            @if(Session::has('alert-' . $message))
            <p class="alert alert-{{ $message }}" style="padding-top:5px;height:30px;">{{ Session::get('alert-' . $message) }}</p>
            @endif
            @endforeach
        </div>
        </div>
    </div>
    <div id="myTable">
        <form class="form-horizontal"  name="student" role="form"  method="POST" action="" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <table class="table table-striped table-hover">
                <thead>
                    <tr> 
                        <th>Subject Name</th>
                        <th>Grade</th>
                    </tr>
                </thead>
                <tbody class="e_tbody">
                    @foreach($lists as $lst)
                        <tr>
                            <td>
                                {{$lst->subject}}
                                <input type="hidden" name="grade_id[]" value="{{$lst->id}}">
                            </td>
                            <td>
                                <input type="text" class="form-control" name="grade_gade[]" value="{{$lst->grade}}" onkeypress="return isNumberKey(event, this);"> 
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                <div class="loading"></div>
            </table>
            <div class="form-group">
                <div class="col-xs-3">
                    <button class="btn btn-lg btn-primary btn-sm update" id="myBtn" type="submit" name="update" value="update"><img src="/images/update1.png">&nbsp;Update</button>
                    <button class="btn btn-lg btn-primary btn-sm print" id="myBtn" type="submit" name="print" value="print"><img src="/images/print1.png">&nbsp;Print</button>
                </div>
            </div>
        </form>
    </div>
    </section>
    <script type="text/javascript">
        function isNumberKey(evt, element){
            var charCode = (evt.which) ? evt.which : event.keyCode
            if ((charCode != 46 || $(element).val().indexOf('.') != -1) && // “.” CHECK DOT, AND ONLY ONE. 
                (charCode < 48 || charCode > 57))
                return false;
            return true;
        }
        $('.print').hover(function(){
            $('form').prop('target','_blank');
        }, function(){
            $('form').removeAttr('target');
        });
    </script>
@endsection
