@extends('layouts.header')
@section('content')
<style type="text/css">
    .inner-addon {
      position: relative;
    }

    /* style glyph */
    .inner-addon .glyphicon {
      position: absolute;
      padding: 10px;
      pointer-events: none;
      color:#336699;
      top:-7px;
    }
    /*tr,td{
        border:1px solid black;
    }*/
    /* align glyph */
    .right-addon .glyphicon { right: 0px;}

    /* add padding  */  
    .right-addon input { padding-right: 30px; }
</style>
  <section class="container main_section">
    <div class="form-group" style="margin-top:20px;">
        <div class="col-xs-11">
        <div class="flash-message">
            @foreach (['danger', 'warning', 'success', 'info'] as $message)
            @if(Session::has('alert-' . $message))
            <p class="alert alert-{{ $message }}" style="padding-top:5px;height:30px;">{{ Session::get('alert-' . $message) }}</p>
            @endif
            @endforeach
        </div>
        </div>
    </div>
    <div id="myTable">
        <table class="table table-striped table-hover">
            <thead>
                <tr> 
                    <th>Name</th>
                    <th>Course</th>
                    <th>Year</th>
                </tr>
            </thead>
            <tbody class="e_tbody">
                @foreach($grades as $grd)
                    <tr>
                        <td>
                            <a href="/webpage/gradeprofile={{$grd->student_id}}"> {{$grd->lname}}, {{$grd->fname}} {{$grd->mname}} </a>
                        </td>
                        <td>{{$grd->coursename}}</td>
                        <td>{{$grd->year}}</td>
                    </tr>
                @endforeach
            </tbody>
            <tfoot class="s_tfoot">
                <div id="page-selection" class="pagination" style=" position:fixed; bottom: 15px; width: 700px;margin-bottom:10px;margin-left:-1000px;">
                    {!! with(new Illuminate\Pagination\BootstrapThreePresenter($grades))->render()!!}
                </div>
            </tfoot> 
            <div class="loading"></div>
        </table>
    </div>
    </section>
    <div class="modal fade" id="addMisc" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content" >
                <div class="modal-header">
                    <h4><b>Payments</b></h4>
                </div>
                <div class="modal-body">
                 <form class="form-horizontal"  name="student" role="form"  method="POST" action="" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                        <label class="control-label col-xs-3">Student ID</label>
                        <div class="col-xs-7">
                            <select class="form-control studentid" name="studentid" data-width="100%" required>
                                <option value="" selected="">--Select--</option>     
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-3">Year</label> 
                        <div class="col-xs-7">
                            <select class="form-control year" name="year" required="">
                                <option value="" selected="">--Select--</option>
                                <option value="First">First</option>
                                <option value="Second">Second</option>
                                <option value="Third">Third</option>
                                <option value="Fourth">Fourth</option>
                            </select>
                        </div>   
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-3">Course</label>
                        <div class="col-xs-7">
                            <select class="form-control course" name="course" data-width="100%" required>
                                <option value="" selected="">--Select--</option>
                                @foreach($courses as $course)
                                    <option value="{{$course->id}}">{{$course->short}}</option>
                                @endforeach     
                            </select>
                        </div>
                    </div>
                    <div class="container">
                    <div class="col-xs-8">
                        <table class="table table-hover table-striped">
                            <thead>
                                <tr>
                                    <th width="5%%"></th>
                                    <th width="50%">Name</th>
                                    <th width="30%">Grade</th>
                                </tr>
                            </thead>
                            <tbody class="subject">
                                
                            </tbody>
                        </table>
                    </div>
                    </div>
                    
                </div>
                <div class="modal-footer">
                    <button class="btn btn-lg btn-primary btn-sm addmisc" id="myBtn" type="submit" name="addmisc" value="addmisc"><img src="/images/save1.png">&nbsp;Save</button>
                </div>
                </form>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $.getJSON('/dev/api/studentlist', function(data){
            data = $.map(data, function(partner){
            return {id: partner.id, text: partner.name };
            });
            $(".studentid").select2({
                minimumInputLength: 1,
                multiple: false, 
                data: data 
            });
        }); 
        $('.course, .year').on('change',function(){
            subje();
        })
        function subje(){
            var cid = $('.course option:selected').val();
            var year = $('.year option:selected').val();
            $('table tbody.subject').empty();
            $.get('/dev/api/searchsubject?cid='+cid+'&year='+year, function(data){
                $.each(data, function(index, dtl){
                    $('table tbody.subject').append('<tr>\
                        <td>\
                            '+dtl.id+'\
                            <input type="hidden" name="sub_id[]" value="'+dtl.id+'">\
                        </td>\
                        <td>\
                            '+dtl.name+'\
                        </td>\
                        <td>\
                            <input type="text" class="form-control" name="grade[]" value="">\
                        </td>\
                    </tr>')
                });
                
            })
        }
    </script>
@endsection
