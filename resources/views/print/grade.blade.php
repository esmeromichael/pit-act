<!DOCTYPE html>
<html>
<head>
	<title>Billing Statement</title>
	<style type="text/css">
		.circle {
			font-size: 8pt;
			text-align: left;
		}
		.circle1 {
			font-size: 8pt;
		}
		.circle2 {
			text-align: right;
			font-size: 8pt;
		}
		
		.left{
			width:75%;
			border: 0.3px solid #333;
		}
		.right{
			width:25%;
			text-align: center;
			border: 0.3px solid #333;
		}
		.border{
			border: 0.3px solid #333;
		}
	</style>
</head>
<body>
<table>
	<tbody><br>
		<tr style="text-align:center;">
			<td width="10%">
			</td>
			<td width="10%">
				<img src="/images/skul.png">
			</td>
			<td style="width: 60%" colspan="4">
					Republic of the Philippines
					<br>
					PALOMPON INSTITUTE OF TECHNOLOGY<br>
					PIT-TABANGO CAMPUS <br>
					Tabango Leyte
				<br/>
			</td>
			<td width="10%"></td>
			<td width="10%"></td>
		</tr> <br>
		<br>
		<tr>
			<td style="text-align:left; width: 50%">
				&nbsp;&nbsp;{{$student->lname}}, {{$student->fname}} {{$student->mname}}
			</td>
		</tr>
	</tbody>
</table> <br><br>
<table>
	<thead style="text-align:center;">
		<tr>
			<th style="border:1px solid black;width:40%;text-align:center;border-bottom: none;">Subject</th>
			<th style="border:1px solid black;width:20%;text-align:center;border-bottom: none;">Grades</th>
		</tr>	
	</thead>
	<tbody>
		@foreach($grades as $grd)
			<tr>
				<td style="border:.25px solid black;width:40%;text-align:center;font-size:9px;">{{$grd->subject}}</td>
				<td style="border:.25px solid black;width:20%;text-align:center;font-size:9px;">{{$grd->grade}}</td>
			</tr>
		@endforeach
	</tbody>
</table>

</body> 
</html>

