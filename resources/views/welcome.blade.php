@extends('layouts.header')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Welcome</div>

                <div class="panel-body">
                    Your Application's Landing Page.
                </div>
            </div>
        </div>
    </div>
</div>




<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">PIT-TC Clinic</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li @if($tab == "Home") style="background-color:#b3d9ff;" @else style="background-color:#ffffff;" @endif>
                    <a href="/home">Home</a>
                </li>
                <li @if($tab == "Student") style="background-color:#b3d9ff;" @else style="background-color:#ffffff;" @endif>
                    <a href="/webpage/index">Student</a>
                </li>
                <li @if($tab == "Employee") style="background-color:#b3d9ff;" @else style="background-color:#ffffff;" @endif>
                    <a href="/webpage/employee">Employee</a>
                </li>
                <li @if($tab == "Consultation") style="background-color:#b3d9ff;" @else style="background-color:#ffffff;" @endif>
                    <a href="/webpage/consultation">Consultation</a>
                </li>
                <li @if($tab == "Medicine") style="background-color:#b3d9ff;" @else style="background-color:#ffffff;" @endif>
                    <a href="/webpage/medicine">Medicine</a>
                </li>
                <li class="dropdown" @if($tab == "Purchase") style="background-color:#b3d9ff;" @else style="background-color:#ffffff;" @endif>
                    <a href="/purchase/index" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Purchase <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="/purchase/index">Request</a></li>
                        <li><a href="/receiving/index">Receiving</a></li>
                        <li><a href="#">Return</a></li>
                    </ul>
                </li>
                <li @if($tab == "Reports") style="background-color:#b3d9ff;" @else style="background-color:#ffffff;" @endif>
                    <a href="/webpage/reports">Reports</a>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                @if(Auth::guest())
                    <li><a href="{{ url('/login') }}" style="color: #ffffff;">Login</a></li>
                    <li><a href="{{ url('/register') }}" style="color: #ffffff;">Register</a></li>
                @else
                @endif
                <li><a href="#">{{ Auth::user()->name }}</a></li>
                <li><a href="{{ url('/logout') }}">Logout</a></li>
                <li>
                    <a href="#">
                        <b><span id="date" style="font-size:13px; color:#99FF66">
                        </span></b> || <i><span id="time" style="font-size:15px; color:#99FF66">
                        </span></i>
                    </a>
                </li>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</nav>
@endsection
