<ul class="nav navbar-nav">
    <li @if($tab == "Home") style="background-color:#b3d9ff;" @else style="background-color:#ffffff;" @endif>
    	<a href="/home">Home</a>
    </li>
    <li @if($tab == "Student") style="background-color:#b3d9ff;" @else style="background-color:#ffffff;" @endif>
    	<a href="/webpage/index">Student</a>
    </li>
    <li @if($tab == "Employee") style="background-color:#b3d9ff;" @else style="background-color:#ffffff;" @endif>
    	<a href="/webpage/employee">Employee</a>
    </li>
    <li @if($tab == "Consultation") style="background-color:#b3d9ff;" @else style="background-color:#ffffff;" @endif>
    	<a href="/webpage/consultation">Consultation</a>
    </li>
    <li @if($tab == "Medicine") style="background-color:#b3d9ff;" @else style="background-color:#ffffff;" @endif>
    	<a href="/webpage/medicine">Medicine</a>
    </li>
    <li @if($tab == "Purchase") style="background-color:#b3d9ff;" @else style="background-color:#ffffff;" @endif>
        <a href="/purchase/index">Purchase</a>
    <li @if($tab == "Reports") style="background-color:#b3d9ff;" @else style="background-color:#ffffff;" @endif>
    	<a href="/webpage/reports">Reports</a>
    </li>
</ul>