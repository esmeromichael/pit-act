@extends('layouts.base')
@section('body_class')
sidebar_hidden side_fixed full_width
@endsection
@section('body')
<style type="text/css">
    li{
        margin-right:2px;
        margin-bottom:4px; 
    }
   /* .main > li{
        background-color: #665851;
    }*/
    body{
        min-height: 2000px;
        padding-top: 70px;
    }
    .glyphicon {
        font-size: 22px;
    }

    .dropdown-menu > li.kopie > a {
    padding-left:5px;
}
 
.dropdown-submenu {
    position:relative;
}
.dropdown-submenu>.dropdown-menu {
   top:0;left:100%;
   margin-top:-6px;margin-left:-1px;
   -webkit-border-radius:0 6px 6px 6px;-moz-border-radius:0 6px 6px 6px;border-radius:0 6px 6px 6px;
 }
  
.dropdown-submenu > a:after {
  border-color: transparent transparent transparent #333;
  border-style: solid;
  border-width: 5px 0 5px 5px;
  content: " ";
  display: block;
  float: right;  
  height: 0;     
  margin-right: -10px;
  margin-top: 5px;
  width: 0;
}
 
.dropdown-submenu:hover>a:after {
    border-left-color:#555;
 }

.dropdown-menu > li > a:hover, .dropdown-menu > .active > a:hover {
  text-decoration: none;
}  
  
@media (max-width: 767px) {

  .navbar-nav  {
     display: inline;
  }
  .navbar-default .navbar-brand {
    display: inline;
  }
  .navbar-default .navbar-toggle .icon-bar {
    background-color: #fff;
  }
  .navbar-default .navbar-nav .dropdown-menu > li > a {
    color: red;
    background-color: #ccc;
    border-radius: 4px;
    margin-top: 2px;   
  }
   .navbar-default .navbar-nav .open .dropdown-menu > li > a {
     color: #333;
   }
   .navbar-default .navbar-nav .open .dropdown-menu > li > a:hover,
   .navbar-default .navbar-nav .open .dropdown-menu > li > a:focus {
     background-color: #ccc;
   }

   .navbar-nav .open .dropdown-menu {
     border-bottom: 1px solid white; 
     border-radius: 0;
   }
  .dropdown-menu {
      padding-left: 10px;
  }
  .dropdown-menu .dropdown-menu {
      padding-left: 20px;
   }
   .dropdown-menu .dropdown-menu .dropdown-menu {
      padding-left: 30px;
   }
   li.dropdown.open {
    border: 0px solid red;
   }

}

@media (min-width: 768px) {
  ul.nav li:hover > ul.dropdown-menu {
    display: block;
  }
  #navbar {
    text-align: center;
  }
}
.navbar-default {
  /*forediting*/
  /*background-color: #333;*/
  background-color: rgb(182, 223, 253, 1);
  border-color: rgb(182, 223, 253, 1);
}
</style>
<div id="wrapper_all">
    <nav class="navbar navbar-default navbar-fixed-top" style="height:70px;">
        <div class="container">
            <div class="navbar-header" style="">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#" style="color: #ffffff;">
                <img style="display: block; margin-left: auto; margin-right: auto;" src="/images/skul.png" alt="" width="50" height="50"></a></a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav main" style="margin-top:16px;">
                    <li @if($tab['tab'] == "Home") style="background-color:#1B1B1B;" @else style="background-color:#665851;" @endif>
                        <a href="/home" style="color: #ffffff;">Home</a> 
                    </li>
                    <li @if($tab['tab'] == "Student") style="background-color:#1B1B1B;" @else style="background-color:#665851;" @endif>
                        <a href="/webpage/index" style="color: #ffffff;">Students</a>
                    </li>
                    <li class="dropdown" @if($tab['tab'] == "Accounting") style="background-color:#1B1B1B;" @else style="background-color:#665851;" @endif>
                        <a href="/webpage/accounting" style="color: #ffffff;">Grades</a>
                    </li>
                    <li class="dropdown" @if($tab['tab'] == "Receipt") style="background-color:#1B1B1B;" @else style="background-color:#665851;" @endif>
                        <a href="/webpage/receipt" style="color: #ffffff;">Enrollments</a>
                    </li>
                    <li class="dropdown" @if($tab['tab'] == "Settings") style="background-color:#1B1B1B;" @else style="background-color:#665851;" @endif>
                      <a href="/webpage/settings" style="color: #ffffff;">Subjects</a>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-right" style="margin-top:6px;">
                    @if(Session::get('user_id'))
                        <li>
                            @include('layouts.righttab')
                        </li>
                        <li><a href="{{ url('/logout') }}" style="color: #ffffff;">Logout</a></li>
                        <li style="display:none;">
                            <a href="#">
                                <b><span id="date" style="font-size:13px; color:#99FF66">
                                </span></b> || <i><span id="time" style="font-size:15px; color:#99FF66">
                                </span></i>
                            </a>
                        </li>
                    @else
                        <li><a href="{{ url('/login') }}" style="color: #ffffff;">Login</a></li>
                        <li><a href="{{ url('/register') }}" style="color: #ffffff;">Register</a></li>
                    @endif
                </ul>
            </div><!--/.nav-collapse  -->
        </div>
    </nav>
    <nav id="mobile_navigation"></nav>
    <section class="container main_section">
        @yield('content')
    </section>
</div>
@endsection
