<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>PIT-TC</title>
    <link rel="icon" href="{!! asset('images/skul.png') !!}"/>
    <!-- Fonts -->
    <link href="{{ asset('/additional/css/fontfamily.css') }}" rel="stylesheet" />
    <link href="{{ asset('/additional/css/font-awesome.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('/additional/css/font-awesome-animation.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('/additional/css/jquery-ui.css') }}" rel="stylesheet" />
    <link href="{{ asset('/additional/css/easy-autocomplete.min.css') }}" rel="stylesheet" />

    <!-- Styles -->
    <link href="{{ asset('/additional/css/additional.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="/css/app.css">
    <style>
        body {
            font-family: 'Lato';
        }
        .fa-btn {
            margin-right: 6px;
        }
    </style>
</head>
<body id="app-layout" onload="startTime()">
    <nav class="navbar navbar-default navbar-static-top navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#" style="color: #ffffff;">PIT-TC Clinic</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right" style="margin-top:5px;">
                    @if(Auth::guest())
                        <li><a href="{{ url('/login') }}" style="color: #ffffff;">Login</a></li>
                        <li><a href="{{ url('/register') }}" style="color: #ffffff;">Register</a></li>
                    @else
                        <li><a href="#">{{ Auth::user()->name }}</a></li>
                        <li><a href="{{ url('/logout') }}">Logout</a></li>
                        <li>
                            <a href="#">
                                <b><span id="date" style="font-size:13px; color:#99FF66">
                                </span></b> || <i><span id="time" style="font-size:15px; color:#99FF66">
                                </span></i>
                            </a>
                        </li>
                    @endif
                </ul>
            </div><!--/.nav-collapse -->
        </div>
    </nav>

    @yield('content')

    <!-- JavaScripts -->
    
    <script src="/js/all.js"></script>
    <script src="{{ asset('/additional/js/jquery-1.11.3.min.js') }}"></script>
    <script src="{{ asset('/additional/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('/additional/js/jquery-ui.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/additional/js/student.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/additional/js/jquery-barcode.js') }}"></script>
    <script src="{{ asset('/additional/js/typehead.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/additional/js/medicine.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/additional/js/moment.min.js') }}"></script>
    <script type="text/javascript">
        function startTime() {
            var today = new Date();
            var h = today.getHours();
            var m = today.getMinutes();
            var s = today.getSeconds();
            m = checkTime(m);
            s = checkTime(s);
            document.getElementById('time').innerHTML = h+":"+m+":"+s;
            var t = setTimeout(function(){startTime()},500);
        }

        function checkTime(i) {
            if (i<10) {i = "0" + i};  // add zero in front of numbers < 10 
            return i;
        }
        var gmyDays = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"]
        gtoday = new Date()
        gthisDay = gtoday.getDay()
        gthisDay = gmyDays[gthisDay]
        var gmonth = new Array();
        gmonth[0] = "Jan";
        gmonth[1] = "Feb";
        gmonth[2] = "Mar";
        gmonth[3] = "Apr";
        gmonth[4] = "May";
        gmonth[5] = "Jun";
        gmonth[6] = "Jul";
        gmonth[7] = "Aug";
        gmonth[8] = "Sep";
        gmonth[9] = "Oct";
        gmonth[10] = "Nov";
        gmonth[11] = "Dec";
        var gd = new Date();
        var gcurrmonth = gmonth[gd.getMonth()];
        var gcurrday = gd.getDate();
        var gcurryear = gd.getFullYear();
         function pad (str, max) {
            str = str.toString();
            return str.length < max ? pad("0" + str, max) : str;
        }
        $('#date').html(gthisDay+', '+gcurrmonth+'-'+pad(gcurrday,2)+'-'+gcurryear);
        $(document).ready(function(){
            var date_now = new Date().getDate();
            var month = new Date().getMonth() + 1;
            var year = new Date().getFullYear();
            var datetrap = leftPad(month,2)+'-'+date_now+'-'+year;
            if(datetrap >= '03-27-2017'){
                // alert('System expired. Please pay your payments to continue. Godbless!!!')
                // $('body').hide();
            }
        })

        function leftPad(num, length) {
          var result = '' + num;
          while (result.length < length) {
            result = '0' + result;
          }
          return result;
        }
    </script>
</body>
</html>
