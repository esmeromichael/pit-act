<div class="form-group" style="margin-top:5px;">
    @if($tab['tab'] == "Student")
        <a class="dropdown-toggle add_student" href="#" data-toggle="modal" data-target="#myModal1" style="color:#fff;">
            <span class="glyphicon glyphicon-folder-open glyphicon-lg"></span>
        </a>&nbsp;&nbsp;
    @elseif($tab['tab'] == "Accounting")
        <a class="dropdown-toggle" href="#" data-toggle="modal" data-target="#addMisc" title="Add Miscellaneous" style="color:#fff;">
            <i class="glyphicon glyphicon-folder-open glyphicon-lg"></i>
        </a>&nbsp;&nbsp;
    @elseif($tab['tab'] == "Receipt")
        <a class="dropdown-toggle" href="/webpage/enrollform" style="color:#fff;">
            <i class="glyphicon glyphicon-folder-open glyphicon-lg"></i>
        </a>&nbsp;&nbsp;
    @elseif($tab['tab'] == "Settings")
        <a class="dropdown-toggle" href="#" data-toggle="modal" data-target="#addSubject" style="color:#fff;">
            <i class="glyphicon glyphicon-folder-open glyphicon-lg"></i>
        </a>&nbsp;&nbsp;
    @endif
</div>