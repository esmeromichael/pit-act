<!DOCTYPE html>
<html>
<head>
    <title>PIT-TC</title>
    <link rel="stylesheet" href="/css/app.css">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="{!! asset('images/skul.png') !!}"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="{{ asset('/additional/css/select2.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('/additional/css/fontfamily.css') }}" rel="stylesheet" />
    <link href="{{ asset('/additional/css/font-awesome.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('/additional/css/font-awesome-animation.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('/additional/css/jquery-ui.css') }}" rel="stylesheet" />
    <link href="{{ asset('/additional/css/easy-autocomplete.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('/additional/css/additional.css') }}" rel="stylesheet" />
        <!--Time-->
    <script language="javascript">
        function startTime() {
            var today = new Date();
            var h = today.getHours();
            var m = today.getMinutes();
            var s = today.getSeconds();
            m = checkTime(m);
            s = checkTime(s);
            document.getElementById('time').innerHTML = h+":"+m+":"+s;
            var t = setTimeout(function(){startTime()},500);
        }

        function checkTime(i) {
            if (i<10) {i = "0" + i};  // add zero in front of numbers < 10
            return i;
        }
    </script>
</head>
<body class="@yield('body_class')" onload="startTime()">
    <script src="/js/all.js"></script>
    <script src="{{ asset('/additional/js/jquery-1.11.3.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/additional/js/select2.min.js') }}"></script>
    <script src="{{ asset('/additional/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('/additional/js/sub/selectize.min.js') }}"></script>
    <script src="{{ asset('/additional/js/jquery-ui.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/additional/js/student.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/additional/js/jquery-barcode.js') }}"></script>
    <script src="{{ asset('/additional/js/typehead.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/additional/js/global.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/additional/js/moment.min.js') }}"></script>
    <script type="text/javascript">
      $.ajaxSetup({
      headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });
    </script>
    @yield('body')
    <!--Date--> 
    <script language="javascript">
        var gmyDays = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"]
        gtoday = new Date()
        gthisDay = gtoday.getDay()
        gthisDay = gmyDays[gthisDay]
        var gmonth = new Array();
        gmonth[0] = "Jan";
        gmonth[1] = "Feb";
        gmonth[2] = "Mar";
        gmonth[3] = "Apr";
        gmonth[4] = "May";
        gmonth[5] = "Jun";
        gmonth[6] = "Jul";
        gmonth[7] = "Aug";
        gmonth[8] = "Sep";
        gmonth[9] = "Oct";
        gmonth[10] = "Nov";
        gmonth[11] = "Dec";
        function pad (str, max) {
            str = str.toString();
            return str.length < max ? pad("0" + str, max) : str;
        }
        var gd = new Date();
        var gcurrmonth = gmonth[gd.getMonth()];
        var gcurrday = gd.getDate();
        var gcurryear = gd.getFullYear();
        $('#date,.rgdatex').html(gthisDay+', '+gcurrmonth+'-'+pad(gcurrday,2)+'-'+gcurryear);
        $("#date").css("cursor","default");
        $("#time").css("cursor","default");
    </script>
</body>
</html>
